# jlab-debugger

Test of the "debugger" extension for JupyterLab using a conda environment and deploying it at [CERN's JupyterHub](https://hub.cern.ch).
